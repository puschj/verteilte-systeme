package de.jojo.server;

import java.util.concurrent.Semaphore;

import de.jojo.rpc.Server;
import de.jojo.rpc.ServerStub;


/**
 * Created with IntelliJ IDEA.
 * User: joachim
 * Date: 11/30/12
 */
public class StartServer {

    public static Semaphore mutex;

    public static void main(String args[]) throws Exception {

        mutex = new Semaphore(1);

        Server rpcServer = new Server();
        rpcServer.addServerStub(new ServerStub(PrintJobManager.getInstance()));
        rpcServer.start();
    }
}
