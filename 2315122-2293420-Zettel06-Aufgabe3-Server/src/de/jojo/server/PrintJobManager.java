package de.jojo.server;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: joachim
 * Date: 11/30/12
 */
public class PrintJobManager implements ResourceManagement {

    private static PrintJobManager instance = null;

    private PrintJobManager() {
    }

    public static PrintJobManager getInstance() {
        if (instance == null) {
            instance = new PrintJobManager();
        }
        return instance;
    }


    @Override
    public boolean request() {
        boolean result = false;

        try {
            StartServer.mutex.acquire();

//            System.out.println(Thread.currentThread().getName() + " inside mutual exclusive region");

            PhysicalPrinter physicalPrinter = PhysicalPrinter.getInstance();
            PhysicalPrinter.setWhatToPrint("printing in Thread " +
                    Thread.currentThread().getName() + ": " + new Random().nextInt());
            result = physicalPrinter.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean release() {
        StartServer.mutex.release();
        return true;
    }
}
