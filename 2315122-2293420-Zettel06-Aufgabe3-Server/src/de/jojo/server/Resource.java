package de.jojo.server;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: joachim
 * Date: 11/30/12
 */
public abstract class Resource implements Callable<Boolean> {

    public final static int SECONDS_NEEDED_FOR_CREATION = 1;

    /**
     * Use this resource. Setting up the usage of the resource takes {@value #SECONDS_NEEDED_FOR_CREATION}
     * seconds.
     *
     * @return true if this Resource could do its work without interruption, else false
     * @throws Exception
     */
    @Override
    public Boolean call() throws Exception {
        System.out.println("Initializing the resource..");
        try {
            Thread.sleep(SECONDS_NEEDED_FOR_CREATION * 1000);
            System.out.println("Resource successfully initialized");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Resource not initialized");
        return false;
    }
}
