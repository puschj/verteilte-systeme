package de.jojo.server;

/**
 * Created with IntelliJ IDEA.
 * User: joachim
 * Date: 11/30/12
 */
public class PhysicalPrinter extends Resource {

    private final static int SECONDS_NEEDED_FOR_SETUP_PRINTER = 2;

    private static String whatToPrint = "";

    private static PhysicalPrinter instance = null;

    private PhysicalPrinter() {
    }

    public static PhysicalPrinter getInstance() {
        if (instance == null) {
            instance = new PhysicalPrinter();
        }
        return instance;
    }

    public static void setWhatToPrint(String text) {
        whatToPrint = text;
    }

    /**
     * {@inheritDoc}
     * Setting up the PhysicalPrinter takes {@value #SECONDS_NEEDED_FOR_SETUP_PRINTER} seconds.
     */
    @Override
    public Boolean call() throws Exception {
        if (super.call() != null) {
            System.out.println("Starting to print..");
            try {

                Thread.sleep(SECONDS_NEEDED_FOR_SETUP_PRINTER * 1000);

                System.out.println("printed on an actual piece of paper:" + "\n" +
                        "======" + "\n" + whatToPrint + "\n" + "======");

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
