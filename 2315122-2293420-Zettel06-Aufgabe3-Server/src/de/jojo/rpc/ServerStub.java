package de.jojo.rpc;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;

public class ServerStub {

    private Object impl;

    public ServerStub(Object impl) {
        this.impl = impl;
    }

    public void processMethodCall(ProcedureCall call, ObjectOutputStream os) {
        Object res;
        try {
            try {
                Method method = impl.getClass().getMethod(call.getProcName(),
                        call.getParameterTypes());
                res = method.invoke(impl, call.getArgs());
            } catch (Exception e) {
                os.writeObject(e);
                os.flush();
                return;
            }
            os.writeObject(res);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Class<?> getImplementationClass() {
        return impl.getClass();
    }
}
