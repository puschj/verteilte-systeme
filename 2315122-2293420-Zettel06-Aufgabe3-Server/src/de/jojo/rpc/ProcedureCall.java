package de.jojo.rpc;

import java.io.Serializable;

class ProcedureCall implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 2428047933457918794L;

  private Class<?> supportedClass;

  private String procName;

  private Class<?>[] parameterTypes;

  private Object[] args;

  public ProcedureCall(Class<?> supportedClass, String procName,
                       Class<?>[] parameterTypes, Object[] args) {
      super();
      this.supportedClass = supportedClass;
      this.procName = procName;
      this.parameterTypes = parameterTypes;
      this.args = args;
  }

  public Class<?> getSupportedClass() {
      return supportedClass;
  }

  public void setSupportedClass(Class<?> supportedClass) {
      this.supportedClass = supportedClass;
  }

  public String getProcName() {
      return procName;
  }

  public void setProcName(String procName) {
      this.procName = procName;
  }

  public Object[] getArgs() {
      return args;
  }

  public void setArgs(Object[] args) {
      this.args = args;
  }

  public Class<?>[] getParameterTypes() {
      return parameterTypes;
  }

  public void setParameterTypes(Class<?>[] parameterTypes) {
      this.parameterTypes = parameterTypes;
  }
}
