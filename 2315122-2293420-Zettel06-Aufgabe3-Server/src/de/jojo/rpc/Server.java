package de.jojo.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Server {

    private static final int PORT = 1055;

    private static final int POOL_SIZE = 8;

    private static final int MAX_POOL_SIZE = 16;

    private List<ServerStub> serverStubs;

    private ThreadPoolExecutor pool;

    private ServerSocket serverSocket;

    public Server() {
        serverStubs = new ArrayList<ServerStub>();
        pool = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, 0,
                TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    }

    public void addServerStub(ServerStub serverStub) {
        serverStubs.add(serverStub);
    }

    public void removeServerStub(ServerStub serverStub) {
        serverStubs.remove(serverStub);
    }

    private class CallHandler implements Runnable {

        private ObjectInputStream is;
        private ObjectOutputStream os;

        public CallHandler(Socket socket) throws IOException {
            os = new ObjectOutputStream(socket.getOutputStream());
            is = new ObjectInputStream(socket.getInputStream());
        }

        @Override
        public void run() {
            ProcedureCall call;
            try {
                Object o = is.readObject();
                call = (ProcedureCall) o;
                for (ServerStub serverStub : serverStubs) {
                    if (call.getSupportedClass().isAssignableFrom(
                            serverStub.getImplementationClass())) {
                        serverStub.processMethodCall(call, os);
                        return;
                    }
                    os.writeObject(new ClassNotFoundException(
                            "no server stub registered for '"
                                    + call.getSupportedClass().getSimpleName()
                                    + "'"));
                    os.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(PORT);
                    while (true) {
                        Socket socket = serverSocket.accept();
                        System.out.println("connection engaged: " + socket);
                        pool.execute(new CallHandler(socket));
                    }
                } catch (IOException e) {
                    System.out.println("server closed (reason: \""
                            + e.getMessage() + "\")");
                }
            }
        }).start();
        System.out.println("server started");
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
