package de.jojo.practice01.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;

public class Server {
	public static void main(String[] args) throws RemoteException {
		LocateRegistry.createRegistry( Registry.REGISTRY_PORT );

	    CipherImpl cipher = new CipherImpl();
	    Cipher skeleton = (Cipher) UnicastRemoteObject.exportObject(cipher, 0 );
	    RemoteServer.setLog( System.out );

	    Registry registry = LocateRegistry.getRegistry();
	    registry.rebind( "Cipher", skeleton );
	}
}
