package de.jojo.practice01.rmi;

import java.rmi.RemoteException;

import de.jojo.practice01.ROT13;

public class CipherImpl implements Cipher {
	
	
	@Override
	public String encrypt(String s) throws RemoteException {
		return ROT13.process(s);
	}

	@Override
	public String decrypt(String s) throws RemoteException {
		return ROT13.process(s);
	}
	
}
