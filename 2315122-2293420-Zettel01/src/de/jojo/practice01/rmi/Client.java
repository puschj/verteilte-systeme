package de.jojo.practice01.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) throws RemoteException,
			NotBoundException {
		Registry registry = LocateRegistry.getRegistry();
		Cipher cipher = (Cipher) registry.lookup("Cipher");

		String input;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter text to encrypt: ");
		input = scanner.next();
		scanner.close();

		String encrypted = cipher.encrypt(input);
		String decrypted = cipher.decrypt(encrypted);

		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
	}
}
