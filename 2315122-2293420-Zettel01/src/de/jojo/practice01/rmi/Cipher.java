package de.jojo.practice01.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

interface Cipher extends Remote {
	public String encrypt(String s) throws RemoteException;
	public String decrypt(String s) throws RemoteException;
}
