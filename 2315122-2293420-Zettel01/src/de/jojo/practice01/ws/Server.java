package de.jojo.practice01.ws;

import javax.xml.ws.Endpoint;

public class Server {

	public static void main(String[] args) {
		Endpoint endpoint = Endpoint.publish(
				"http://localhost:8080/CipherService", new Cipher());

		System.out.println("CipherService ready");
	}
}
