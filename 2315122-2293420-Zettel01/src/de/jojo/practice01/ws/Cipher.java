package de.jojo.practice01.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import de.jojo.practice01.ROT13;

@WebService(name="Cipher")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class Cipher {
	
	
	@WebMethod
	public String encrypt(@WebParam(name="s") String s) {
		return ROT13.process(s);
	}
	
	@WebMethod
	public String decrypt(@WebParam(name="s") String s) {
		return ROT13.process(s);
	}
}
