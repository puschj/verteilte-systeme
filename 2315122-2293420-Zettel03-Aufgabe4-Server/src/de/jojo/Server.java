package de.jojo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public final static int ENCRYPT_PORT = 1258;
	public final static int DECRYPT_PORT = 1478;

	static ServerSocket encryptServerSocket;
	static ServerSocket decryptServerSocket;

	public static void main(String args[]) throws Exception {

		encryptServerSocket = new ServerSocket(ENCRYPT_PORT);
		decryptServerSocket = new ServerSocket(DECRYPT_PORT);

		System.out.println("Server started using ports " + ENCRYPT_PORT
				+ " and " + DECRYPT_PORT);

		startListening();
	}

	/**
	 * Start listening to new clients on both ports
	 */
	private static void startListening() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (encryptServerSocket != null) {
					try {
						final Socket clientSocket = encryptServerSocket
								.accept();

						/*
						 * After accepting a client, this process should not be
						 * blocked because it is busy handling the client, so
						 * doing the handling of the client in another Thread.
						 */
						new ClientHandlerThread(
								ClientHandlerThread.METHOD_ENCRYPT,
								clientSocket).start();

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (decryptServerSocket != null) {
					try {
						final Socket clientSocket = decryptServerSocket
								.accept();

						/*
						 * After accepting a client, this process should not be
						 * blocked because it is busy handling the client, so
						 * doing the handling of the client in another Thread.
						 */
						new ClientHandlerThread(
								ClientHandlerThread.METHOD_DECRYPT,
								clientSocket).start();

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
