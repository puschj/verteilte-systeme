package de.jojo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import de.umr.encryption.Rot13;

class ClientHandlerThread extends Thread {

	public final static int METHOD_ENCRYPT = 0;
	public final static int METHOD_DECRYPT = 1;

	protected int method;
	private Socket clientSocket;

	/**
	 * @param method
	 * @param clientSocket
	 */
	public ClientHandlerThread(int method, Socket clientSocket) {
		super();
		this.method = method;
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {

		BufferedReader inFromClient;
		try {
			inFromClient = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));

			DataOutputStream outToClient = new DataOutputStream(
					clientSocket.getOutputStream());

			while (true) {
				String input = inFromClient.readLine();

				if (input == null || input.isEmpty()) {
					continue;
				}

				String processed = crypt(input, method);

				/*
				 * when writing out, the "\n" is important, because otherwise
				 * the function BufferedReader.readLine() of the Client will not
				 * terminate
				 */
				outToClient.writeBytes(processed + '\n');
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * En- or decrypt text
	 * 
	 * @param text
	 * @param cryptMethod
	 *            The method to use. 0 for encryption, otherwise decryption
	 * @return a encrypted or decrypted String or "" if no input available
	 */
	private static String crypt(String text, int cryptMethod) {
		if (text != null && !text.isEmpty()) {
			if (cryptMethod == 0) {
				return Rot13.encrypt(text);
			}
			return Rot13.decrypt(text);
		}
		return "";
	}

}