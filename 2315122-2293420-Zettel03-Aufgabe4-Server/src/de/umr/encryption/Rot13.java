package de.umr.encryption;

public class Rot13 {
	public static String encrypt(String cleartext) {
		char[] clear = cleartext.toCharArray();
		char[] cipher = new char[clear.length];

		for (int i = 0; i < clear.length; i++) {
			if (clear[i] >= 'A' && clear[i] <= 'Z')
				cipher[i] = (char) (((clear[i] - 'A' + 13) % 26) + 'A');
			else if (clear[i] >= 'a' && clear[i] <= 'z')
				cipher[i] = (char) (((clear[i] - 'a' + 13) % 26) + 'a');
			else
				cipher[i] = clear[i];
		}

		return new String(cipher);
	}

	public static String decrypt(String ciphertext) {
		return encrypt(ciphertext);
	}
}
