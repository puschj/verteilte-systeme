package de.umr.fb12.vs.registration;

public class Client {

    public static void main(String[] args) {
        Registration r = new RegistrationService().getRegistrationPort();
        String jon = r.register("Jonas", "Pusch", "puschj@mathematik.uni-marburg.de", 2293420);
        String joa = r.register("Joachim", "Reiß", "@", );
        System.out.println(jon);
        System.out.println(joa);
    }
}
