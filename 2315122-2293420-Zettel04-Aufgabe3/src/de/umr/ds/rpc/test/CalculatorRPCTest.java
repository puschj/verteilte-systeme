package de.umr.ds.rpc.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de.umr.ds.rpc.Calculator;
import de.umr.ds.rpc.client.CalculatorClientStub;
import de.umr.ds.rpc.impl.CalculatorImpl;
import de.umr.ds.rpc.server.Server;
import de.umr.ds.rpc.server.ServerStub;

public class CalculatorRPCTest {
    
    private static Server s;
    
    @BeforeClass
    public static void startServer() {
        s = new Server();
        s.addServerStub(new ServerStub(new CalculatorImpl()));
        s.start();
    }
    
    @AfterClass
    public static void stopServer() {
        s.stop();
    }

    @Test
    public void testAdd() {
        Calculator c = new CalculatorClientStub();
        assertEquals(10, c.add(4, 6));
        assertEquals(2, c.add(-4, 6));
        assertEquals(-2, c.add(4, -6));
    }

    @Test
    public void testSub() {
        Calculator c = new CalculatorClientStub();
        assertEquals(2, c.sub(4, 2));
        assertEquals(6, c.sub(4, -2));
        assertEquals(-6, c.sub(-4, 2));
    }

    @Test
    public void testMul() {
        Calculator c = new CalculatorClientStub();
        assertEquals(24, c.mul(4, 6));
        assertEquals(0, c.mul(0, Integer.MAX_VALUE));
        assertEquals(-10, c.mul(2, -5));
    }

    @Test(expected = RuntimeException.class)
    public void testDiv() {
        Calculator c = new CalculatorClientStub();
        assertEquals(2, c.div(10, 5));
        assertEquals(0, c.div(5, 10));
        c.div(10, 0);
        fail("Division by 0 did not return RuntimeException.");
    }

    @Test
    public void testMod() {
        Calculator c = new CalculatorClientStub();
        assertEquals(2, c.mod(6, 4));
        assertEquals(4, c.mod(4, 6));
        assertEquals(0, c.mod(0, 2));
    }

}
