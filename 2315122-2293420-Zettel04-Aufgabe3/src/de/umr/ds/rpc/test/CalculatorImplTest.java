package de.umr.ds.rpc.test;

import static org.junit.Assert.*;

import org.junit.Test;

import de.umr.ds.rpc.Calculator;
import de.umr.ds.rpc.impl.CalculatorImpl;

public class CalculatorImplTest {

    @Test
    public void testAdd() {
        Calculator c = new CalculatorImpl();
        assertEquals(10, c.add(4, 6));
        assertEquals(2, c.add(-4, 6));
        assertEquals(-2, c.add(4, -6));
    }

    @Test
    public void testSub() {
        Calculator c = new CalculatorImpl();
        assertEquals(2, c.sub(4, 2));
        assertEquals(6, c.sub(4, -2));
        assertEquals(-6, c.sub(-4, 2));
    }

    @Test
    public void testMul() {
        Calculator c = new CalculatorImpl();
        assertEquals(24, c.mul(4, 6));
        assertEquals(0, c.mul(0, Integer.MAX_VALUE));
        assertEquals(-10, c.mul(2, -5));
    }

    @Test(expected = ArithmeticException.class)
    public void testDiv() {
        Calculator c = new CalculatorImpl();
        assertEquals(2, c.div(10, 5));
        assertEquals(0, c.div(5, 10));
        c.div(10, 0);
        fail("Division by 0 did not return ArithmeticException.");
    }

    @Test
    public void testMod() {
        Calculator c = new CalculatorImpl();
        assertEquals(2, c.mod(6, 4));
        assertEquals(4, c.mod(4, 6));
        assertEquals(0, c.mod(0, 2));
    }

}
