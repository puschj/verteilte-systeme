package de.umr.ds.rpc.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de.umr.ds.rpc.StringComparator;
import de.umr.ds.rpc.client.StringComparatorClientStub;
import de.umr.ds.rpc.impl.StringComparatorImpl;
import de.umr.ds.rpc.server.Server;
import de.umr.ds.rpc.server.ServerStub;

public class StringComparatorRPCTest {
    
    private static Server s;
    
    @BeforeClass
    public static void startServer() {
        s = new Server();
        s.addServerStub(new ServerStub(new StringComparatorImpl()));
        s.start();
    }
    
    @AfterClass
    public static void stopServer() {
        s.stop();
    }
    
    @Test
    public void testStringsEqual() {
        StringComparator sc = new StringComparatorClientStub();
        assertTrue(sc.stringsEqual("", ""));
        assertTrue(sc.stringsEqual("Hallo", "Hallo"));
        assertFalse(sc.stringsEqual("Hallo", "Welt"));
        assertFalse(sc.stringsEqual("Test", "TEst"));
    }

}
