package de.umr.ds.rpc.client;

import de.umr.ds.rpc.ProcedureCall;
import de.umr.ds.rpc.StringComparator;

public class StringComparatorClientStub extends ClientStub implements
		StringComparator {

	@Override
	public boolean stringsEqual(String s1, String s2) {
		return (Boolean) sendAndReceiveProcedureCall("stringsEqual", s1, s2);
	}

	@Override
	protected ProcedureCall createProcedureCall(String procName, Object[] args) {
		for (Object o : args) {
			if (!(o instanceof String)) {
				return null;
			}
		}

		Class<?>[] parameterTypes = { String.class, String.class };
		return new ProcedureCall(StringComparator.class, procName,
				parameterTypes, args);
	}
}
