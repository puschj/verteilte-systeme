package de.umr.ds.rpc.client;

import de.umr.ds.rpc.Calculator;
import de.umr.ds.rpc.ProcedureCall;

public class CalculatorClientStub extends ClientStub implements Calculator {

	@Override
	public int add(int x, int y) {
		return (Integer) sendAndReceiveProcedureCall("add", x, y);
	}

	@Override
	public int sub(int x, int y) {
		return (Integer) sendAndReceiveProcedureCall("sub", x, y);
	}

	@Override
	public int mul(int x, int y) {
		return (Integer) sendAndReceiveProcedureCall("mul", x, y);
	}

	@Override
	public int div(int x, int y) {
		return (Integer) sendAndReceiveProcedureCall("div", x, y);
	}

	@Override
	public int mod(int x, int y) {
		return (Integer) sendAndReceiveProcedureCall("mod", x, y);
	}

	@Override
	protected ProcedureCall createProcedureCall(String procName, Object[] args) {
		for (Object o : args) {
			if (!(o instanceof Integer)) {
				return null;
			}
		}

		Class<?>[] parameterTypes = { int.class, int.class };
		return new ProcedureCall(Calculator.class, procName, parameterTypes,
				args);
	}

}
