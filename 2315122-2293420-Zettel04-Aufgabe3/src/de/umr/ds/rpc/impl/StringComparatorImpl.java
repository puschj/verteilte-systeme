package de.umr.ds.rpc.impl;

import de.umr.ds.rpc.StringComparator;

public class StringComparatorImpl implements StringComparator {

    @Override
    public boolean stringsEqual(String s1, String s2) {
        return s1.equals(s2);
    }

}
