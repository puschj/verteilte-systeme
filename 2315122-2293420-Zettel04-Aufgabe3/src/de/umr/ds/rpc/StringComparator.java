package de.umr.ds.rpc;

public interface StringComparator {
	boolean stringsEqual(String s1, String s2);
}
