package de.jojo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

class Client {

	public final static int ENCRYPT_PORT = 1258;
	public final static int DECRYPT_PORT = 1478;

	public final static String HOSTNAME = "localhost";

	private static Socket encryptionSocket;
	private static Socket decryptionSocket;

	static DataOutputStream encryptionOutToServer;
	static BufferedReader encryptionInFromServer;

	static DataOutputStream decryptionOutToServer;
	static BufferedReader decryptionInFromServer;

	public static void main(String args[]) throws Exception {
		print("Hello.");
		if (!initConnection()) {
			print("Couldn't init sockets");
		} else {
			doClientWork();
		}
		print("Good bye.");
	}

	private static void doClientWork() {
		ArrayList<String> citation = new ArrayList<String>();
		citation.add("Warum ist ueberhaupt etwas");
		citation.add("und nicht vielmehr nichts?");

		for (String secretText : citation) {

			// encryption
			String encr = requestEncryption(secretText);
			if (encr.isEmpty()) {
				print("Received empty response..");
				return;
			}
			print("Using encryption service of the server resulted in: '"
					+ encr + "'");

			// decryption
			String decr = requestDecryption(encr);
			if (decr.isEmpty()) {
				print("Received empty response..");
				return;
			}
			print("Using decryption service of the server results in:'" + decr
					+ "'");
		}
	}

	/**
	 * 
	 * @param text
	 *            the text to send
	 * 
	 * @return the received text or "" if something went wrong
	 */
	private static String requestEncryption(String text) {
		try {
			encryptionOutToServer.writeBytes(text + '\n');
			String received = encryptionInFromServer.readLine();

			return received;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 
	 * @param text
	 *            the text to send
	 * 
	 * @return the received text or "" if something went wrong
	 */
	private static String requestDecryption(String text) {
		try {
			decryptionOutToServer.writeBytes(text + '\n');
			String received = decryptionInFromServer.readLine();

			return received;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private static boolean initConnection() {

		try {
			encryptionSocket = new Socket(HOSTNAME, ENCRYPT_PORT);
			encryptionOutToServer = new DataOutputStream(
					encryptionSocket.getOutputStream());
			encryptionInFromServer = new BufferedReader(new InputStreamReader(
					encryptionSocket.getInputStream()));

			decryptionSocket = new Socket(HOSTNAME, DECRYPT_PORT);
			decryptionOutToServer = new DataOutputStream(
					decryptionSocket.getOutputStream());
			decryptionInFromServer = new BufferedReader(new InputStreamReader(
					decryptionSocket.getInputStream()));

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static void print(String text) {
		System.out.println(text + "\n");
	}
}