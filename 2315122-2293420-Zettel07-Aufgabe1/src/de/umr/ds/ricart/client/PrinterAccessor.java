package de.umr.ds.ricart.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import de.umr.ds.ricart.Printer;

public class PrinterAccessor {

    private enum State {
        HOLDING, WANTING, UNINTERESTED
    }

    private static final String OK = "OK";

    private State state = State.UNINTERESTED;

    private Long id;

    private int port;

    private ServerSocket sSocket;

    private Map<Long, SocketAddress> clients;

    private Long requestTime;

    private BlockingQueue<ObjectOutputStream> queue = new LinkedBlockingQueue<ObjectOutputStream>();

    private String coordinatorAddress;

    private int coordinatorPort;

    public PrinterAccessor(Long id, int port, String coordinatorAddress,
            int coordinatorPort) {
        this.id = id;
        this.port = port;
        this.coordinatorPort = coordinatorPort;
        this.coordinatorAddress = coordinatorAddress;
    }

    public void register() {
        Socket socket = null;
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        try {
            socket = new Socket(coordinatorAddress, coordinatorPort);
            socket.setSoTimeout(10000);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            System.out.println("[PA-" + id + "] registering at server");
            out.writeObject("REGISTER");
            out.writeObject(id);
            out.writeObject(port);
            out.flush();

            String ack = (String) in.readObject();
            if (!ack.equals(OK))
                throw new RuntimeException("Registration failed.");

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void refreshClientList() {
        Socket socket = null;
        try {
            socket = new Socket(coordinatorAddress, coordinatorPort);
            ObjectOutputStream out = new ObjectOutputStream(
                    socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(
                    socket.getInputStream());

            out.writeObject("CLIENTS");
            out.flush();
            clients = (Map<Long, SocketAddress>) in.readObject();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String createMessage(Object o) {
        StringBuilder sb = new StringBuilder();
        sb.append(id);
        sb.append(" ");
        sb.append(o);
        return sb.toString();
    }

    public void request() {
        state = State.WANTING;
        requestTime = Calendar.getInstance().getTimeInMillis();
        final String m = createMessage(requestTime);
        System.out.println("[PA-" + id + "] UNINTERESTED -> WANTING");
        ThreadPoolExecutor p = new ThreadPoolExecutor(4, Math.max(4,
                clients.size()), 0, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(clients.size()));

        List<Callable<Boolean>> tasks = new ArrayList<Callable<Boolean>>(
                clients.size());

        for (final Entry<Long, SocketAddress> client : clients.entrySet()) {
            if (!client.getKey().equals(id)) {
                tasks.add(new Callable<Boolean>() {
                    @Override
                    public Boolean call() {
                        Socket socket = null;
                        try {
                            socket = new Socket();
                            socket.connect(client.getValue());
                            ObjectOutputStream out = new ObjectOutputStream(
                                    socket.getOutputStream());
                            ObjectInputStream in = new ObjectInputStream(socket
                                    .getInputStream());
                            out.writeObject(m);
                            out.flush();

                            String ok = (String) in.readObject();
                            if (!ok.equals(OK)) {
                                throw new Exception(
                                        "[PA-"
                                                + id
                                                + "] Received illegal message, while waiting for OK: "
                                                + ok);
                            } else {
                                System.out.println("[PA-" + id
                                        + "] Received OK from "
                                        + socket.getRemoteSocketAddress());
                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return null;
                    }
                });
            }
        }
        try {
            p.invokeAll(tasks);
            state = State.HOLDING;
            System.out.println("[PA-" + id + "] WANTING -> HOLDING");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        for (ObjectOutputStream out : queue) {
            try {
                out.writeObject(OK);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("[PA-" + id + "] HOLDING -> UNINTERESTED");
        state = State.UNINTERESTED;
    }

    public Printer getPrinter() {
        try {
            return new Printer() {

                private Printer p = (Printer) LocateRegistry.getRegistry()
                        .lookup("Printer");

                @Override
                public void print(String s) throws RemoteException {
                    if (state != State.HOLDING) {
                        throw new IllegalStateException(
                                "Called print method while no access to printer was granted.");
                    } else {
                        p.print(s);
                    }
                }
            };
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void start() {
        try {
            sSocket = new ServerSocket(port);
            System.out.println("[PA-" + id + "] started: " + sSocket);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Socket socket = sSocket.accept();
                            new Thread(new ConnectionHandler(socket)).start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    private class ConnectionHandler implements Runnable {

        private Socket socket;

        public ConnectionHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                ObjectOutputStream out = new ObjectOutputStream(
                        socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(
                        socket.getInputStream());

                String[] message = ((String) in.readObject()).split(" ", 2);
                Long senderId = Long.parseLong(message[0]);
                String text = message[1];
                SocketAddress senderAddr = socket.getRemoteSocketAddress();
                System.out.println("[PA-" + id + "] Received " + text
                        + " from " + senderId + " (" + senderAddr + ")");

                // if (senderId != id) {
                switch (state) {
                case HOLDING:
                    if (text.equals(OK)) {
                        throw new RuntimeException(
                                "Received \"OK\" while holding ressource.");
                    } else {
                        queue.put(out);
                        System.out.println("[PA-" + id + "] Put " + senderId
                                + " (" + senderAddr + ")" + " in queue.");
                    }
                    break;
                case WANTING:
                    if (text.equals(OK)) {
                        throw new RuntimeException(
                                "Should not receive OK on that socket.");
                    } else {
                        Long t = Long.parseLong(text);
                        // compare timestamps
                        if (t < requestTime) {
                            // sendMessage(OK, senderAddr);
                            out.writeObject(OK);
                            out.flush();
                            System.out.println("[PA-" + id
                                    + "] Sending OK to: " + senderId + " ("
                                    + senderAddr + ")");
                            // decide by processId, if timestamps are equal
                        } else if (Long.parseLong(text) == requestTime
                                && senderId < id) {
                            out.writeObject(OK);
                            out.flush();
                            System.out.println("[PA-" + id
                                    + "] Sending OK to: " + senderId + " ("
                                    + senderAddr + ")");
                            // sendMessage(OK, senderAddr);
                        } else {
                            queue.add(out);
                            System.out.println("[PA-" + id + "] Put "
                                    + senderId + " (" + senderAddr + ")"
                                    + " in queue.");
                        }
                    }
                    break;
                case UNINTERESTED:
                    if (text.equals(OK)) {
                        throw new RuntimeException(
                                "Received \"OK\" while UNINTERESTED.");
                    } else {
                        // sendMessage(OK, senderAddr);
                        out.writeObject(OK);
                        out.flush();
                        System.out.println("[PA-" + id + "] Sending OK to "
                                + senderId + " (" + senderAddr + ")");
                    }
                    break;
                }
                // }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
