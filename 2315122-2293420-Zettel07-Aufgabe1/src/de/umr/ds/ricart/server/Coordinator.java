package de.umr.ds.ricart.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

public class Coordinator {
    
    private static final String OK = "OK";
    
    private ServerSocket serverSocket;
    
    private Map<Long, SocketAddress> clientAddresses = new HashMap<Long, SocketAddress>();

    private int port;
    
    public Coordinator(int port) {
        this.port = port;
    }
    
    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(port);
                    System.out.println("[Coordinator] started: "+serverSocket);
                    while(true) {
                        try {
                            Socket s = serverSocket.accept();
                            new Thread(new ConnectionHandler(s)).start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }).start();
    }
    
    private class ConnectionHandler implements Runnable {
        
        private Socket socket;
        
        public ConnectionHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                
                String s;
                while ((s = (String) in.readObject()) != null) {
                    if (s.equals("CLIENTS")) {
                        out.writeObject(clientAddresses);
                        out.flush();
                    }
                    if (s.equals("REGISTER")) {
                        Long id = (Long) in.readObject();
                        int port = (Integer) in.readObject();
                        SocketAddress addr = new InetSocketAddress(socket.getInetAddress(), port);
                        clientAddresses.put(id, addr);
                        out.writeObject(OK);
                        out.flush();
                        System.out.println("[Coordinator] registered client: "+id+": "+addr);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
