package de.umr.ds.ricart.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import de.umr.ds.ricart.Printer;

public class PrinterProcess {

  public void start() throws RemoteException {
      LocateRegistry.createRegistry( Registry.REGISTRY_PORT );

      PrinterImpl printer = new PrinterImpl();
      Printer stub = (Printer) UnicastRemoteObject.exportObject(printer, 0 );
//      RemoteServer.setLog( System.out );

      Registry registry = LocateRegistry.getRegistry();
      registry.rebind( "Printer", stub );
  }
}
