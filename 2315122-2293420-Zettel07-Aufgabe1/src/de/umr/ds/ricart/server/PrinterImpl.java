package de.umr.ds.ricart.server;

import java.util.Random;

import de.umr.ds.ricart.Printer;

public class PrinterImpl implements Printer {
    
    private static final int INIT_TIME = 1000;
    
    private static final int MAX_PRINT_TIME = 2000;

    public void print(String s) {
        Random r = new Random();
        int printTime = INIT_TIME + r.nextInt(MAX_PRINT_TIME);
        System.out.println("[Printer] Printing for "+printTime+"ms ...");
        System.out.println("[Printer] " + s);
        try {
            Thread.sleep(printTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[Printer] Printing finished.");
    }
}
