package de.umr.ds.ricart;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Printer extends Remote {
    
    public void print(String s) throws RemoteException;
}
