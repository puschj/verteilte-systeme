package de.umr.ds.ricart;

import java.rmi.RemoteException;

import de.umr.ds.ricart.client.PrinterAccessor;
import de.umr.ds.ricart.server.Coordinator;
import de.umr.ds.ricart.server.PrinterProcess;

public class Main {
    public static void main(String[] args) throws RemoteException {
        PrinterProcess p = new PrinterProcess();
        p.start();
        
        Coordinator c = new Coordinator(1055);
        c.start();
        
        final PrinterAccessor p1 = new PrinterAccessor(1L, 1056, "localhost", 1055);
        final PrinterAccessor p2 = new PrinterAccessor(2L, 1057, "localhost", 1055);
        final PrinterAccessor p3 = new PrinterAccessor(3L, 1058, "localhost", 1055);
        
        p1.register();
        p2.register();
        p3.register();
        
        p1.refreshClientList();
        p2.refreshClientList();
        p3.refreshClientList();
        
        p1.start();
        p2.start();
        p3.start();
        
        Thread p1_print = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    p1.request();
                    p1.getPrinter().print("This is P1's turn.");
                    p1.release();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        
        Thread p2_print = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    p2.request();
                    p2.getPrinter().print("This is P2's turn.");
                    p2.release();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        
        Thread p3_print = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    p3.request();
                    p3.getPrinter().print("This is P3's turn.");
                    p3.release();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        
        p1_print.start();
        
        try {
            Thread.sleep(10);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        
        p2_print.start();
        p3_print.start();
    }
}
