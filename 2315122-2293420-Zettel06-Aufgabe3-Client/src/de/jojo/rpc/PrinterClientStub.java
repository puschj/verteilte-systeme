package de.jojo.rpc;

import de.jojo.server.PrintJobManager;

/**
 * Created with IntelliJ IDEA. User: joachim Date: 11/30/12
 */
public class PrinterClientStub extends ClientStub implements ResourceManagement {

	@Override
	protected ProcedureCall createProcedureCall(String procName, Object[] args) {

		Class<?>[] parameterTypes = {};
		return new ProcedureCall(PrintJobManager.class, procName,
				parameterTypes, args);
	}

	@Override
	public boolean request() {
		System.out.println("requesting resource..");
		return (Boolean) sendAndReceiveProcedureCall("request");
	}

	@Override
	public boolean release() {
		System.out.println("releasing resource..");
		return (Boolean) sendAndReceiveProcedureCall("release");
	}
}
