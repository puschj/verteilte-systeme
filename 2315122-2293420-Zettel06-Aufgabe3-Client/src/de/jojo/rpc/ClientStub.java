package de.jojo.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

abstract class ClientStub {

    private final static int PRINTER_PORT = 1055;

    private static final String ADDRESS = "localhost";

    /**
     * Create a respective ProcedureCall.
     *
     * @param procName the name of the method
     * @param args     the arguments of the method
     * @return
     */
    protected abstract ProcedureCall createProcedureCall(String procName,
                                                         Object[] args);

    /**
     * Send an RPC. Make sure to have a consistent procName/args relationship.
     * Otherwise this will return null before trying to send.
     *
     * @param procName the name of the procedure
     * @param args     the arguments of the method
     * @return the received Object or null
     */
    protected Object sendAndReceiveProcedureCall(String procName,
                                                 Object... args) {
        Object res = null;
        Socket socket = null;
        try {
            // setup connection
            socket = new Socket(ADDRESS, PRINTER_PORT);
            ObjectOutputStream os = new ObjectOutputStream(
                    socket.getOutputStream());
            ObjectInputStream is = new ObjectInputStream(
                    socket.getInputStream());

            // prepare package
            ProcedureCall call = createProcedureCall(procName, args);

            if (call == null) {
                throw new Exception("method name doesn't match the arguments");
            }

            // send
            os.writeObject(call);
            os.flush();

            // receive
            res = is.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // close connection
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }
}
