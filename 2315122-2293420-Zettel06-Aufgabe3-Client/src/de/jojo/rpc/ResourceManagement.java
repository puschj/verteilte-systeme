package de.jojo.rpc;

/**
 * Created with IntelliJ IDEA.
 * User: joachim
 * Date: 11/30/12
 */
public interface ResourceManagement {

    public boolean request();

    public boolean release();

}
