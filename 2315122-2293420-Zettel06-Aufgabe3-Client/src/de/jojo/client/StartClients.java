package de.jojo.client;

import de.jojo.rpc.PrinterClientStub;

import java.util.Random;

/**
 * Created with IntelliJ IDEA. User: joachim Date: 11/30/12
 */
public class StartClients {

	public static void main(String[] args) {

		System.out.println("Hello.");

		for (int i = 0; i < 3; i++) {

			final int threadNum = i;

			new Thread(new Runnable() {

				@Override
				public void run() {
					PrinterClientStub printerClientStub = new PrinterClientStub();

					int seconds = new Random().nextInt(7) + 1;
					System.out.println("waiting " + seconds
							+ " seconds before requesting resource in thread #"
							+ threadNum);
					try {
						Thread.sleep(seconds * 1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					if (printerClientStub.request()) {

						System.out.println("request acknowledged in thread #"
								+ threadNum);

						try {
							seconds = new Random().nextInt(8) + 1;
							System.out.println("holding the resource and "
									+ "sleeping " + seconds
									+ " seconds in thread #" + threadNum);
							Thread.sleep(seconds * 1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						if (printerClientStub.release()) {
							System.out
									.println("release() sent and acknowledged in thread #"
											+ threadNum);
						} else {
							System.out.println("release() failed for thread #"
									+ threadNum);
						}
					} else {
						System.out.println("request() failed for thread #"
								+ threadNum);
					}
				}
			}).start();
		}

	}

}
