/////////////////////////////////////////////////////////////////////
//                                                                 //
//               Verteilte Systeme Zettel 08                       //
//                                                                 //
//               Joachim Rei�, 2315122                             //
//               Jonas Pusch,  2293420                             //
//                                                                 //
/////////////////////////////////////////////////////////////////////

Aufgabe 1) Global State

a) Was ist das Global-State-Problem

Das Problem besteht darin, dass ein Betriebssystem auf einem
Rechner in einem verteilten System, nur den State der lokalen Pro-
zesse wissen kann. Will man den State von Prozessen, die auf anderen
Rechnern im verteilten System laufen, wissen, muss man dabei auf
Prozesskommunikation per Nachrichten zur�ckgreifen. Diese Nachrichten
representieren allerdings nur den Zustand zum Zeitpunkt des Sendens.


b) Snapshot Consistency 

1 - consistent
2 - inconsistent
3 - weakly consistent
4 - weakly consistent

c) Chandy-Lamport



Aufgabe 2) Naming-Service

a) Explain  DNS
    
    
b) Nameserver

 www.bbc.co.uk - 
news.bbc.co.uk -
