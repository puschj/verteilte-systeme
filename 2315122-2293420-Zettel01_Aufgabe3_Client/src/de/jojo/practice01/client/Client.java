package de.jojo.practice01.client;

import java.util.Scanner;

import de.jojo.practice01.ws.Cipher;
import de.jojo.practice01.ws.CipherService;

public class Client {

	public static void main(String[] args) {
		Cipher cipher = new CipherService().getCipherPort();

		String input;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter text to encrypt: ");
		input = scanner.next();
		scanner.close();

		String encrypted = cipher.encrypt(input);
		String decrypted = cipher.decrypt(encrypted);

		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
	}
}
