package de.umr.ds.berkeley;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TimeDaemon extends TimeDevice {
    
    private static InetSocketAddress DAEMON = new InetSocketAddress("Daemon", 0);

    public TimeDaemon(String groupAddr) {
        super(groupAddr);
    }

    public void synchronize() {
        System.out.println("============== BERKELEY SYNCHRONIZATION ALGORITHM ==============");
        MulticastSocket socket;
        try {
            socket = new MulticastSocket(TIME_PORT);
            byte[] buf = new byte[1024];
            DatagramPacket rcv = new DatagramPacket(buf, buf.length);
            Map<InetSocketAddress, Long> differences = new HashMap<InetSocketAddress, Long>();

            // send current date
            long curTime = getTimeInMillis();
            String timeStr = Long.toString(curTime);
            DatagramPacket snd = new DatagramPacket(timeStr.getBytes(),
                    timeStr.length(), group, TIME_PORT);
            System.out.printf("[%10s] %30s %20s\n", "Daemon",
                    "Sending current time:", timeStr);
            socket.send(snd);

            // receive client dates
            try {
                socket.setSoTimeout(1000);
                while (true) {
                    socket.receive(rcv);
                    String diffStr = new String(rcv.getData(), 0,
                            rcv.getLength());
                    differences.put(
                            new InetSocketAddress(rcv.getAddress(), rcv
                                    .getPort()), Long.parseLong(diffStr));
                }
            } catch (SocketTimeoutException e) {
                // include deamon time difference
                differences.put(DAEMON, 0L);
            }

            // calculating correction values
            Map<InetSocketAddress, Long> correctionValues = calcCorrectionValues(differences);
            System.out.printf("[%10s] %30s %s\n", "Daemon",
                    "Sending correction values:", correctionValues);
            for (Entry<InetSocketAddress, Long> entry : correctionValues
                    .entrySet()) {
                long correctionValue = entry.getValue();
                String corrStr = Long.toString(correctionValue);
                InetSocketAddress addr = entry.getKey();
                if (addr != DAEMON) {
                    // send correction values
                    snd = new DatagramPacket(corrStr.getBytes(),
                            corrStr.length(), addr.getAddress(), addr.getPort());
                    socket.send(snd);
                } else {
                    // adjust local time
                    if (correctionValue > Integer.MAX_VALUE
                            || correctionValue < Integer.MIN_VALUE)
                        throw new RuntimeException("correction value overflow");
                    offset += correctionValue;
                    for (TimeListener l : timeListeners)
                        l.timeUpdated(correctionValue);
                }
            }
            System.out.printf("[%10s] %30s %20d\n", "Daemon",
                    "Adjusted time is:", getTime().getTime());
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Map<InetSocketAddress, Long> calcCorrectionValues(
            Map<InetSocketAddress, Long> differences) {
        Map<InetSocketAddress, Long> changeValues = new HashMap<InetSocketAddress, Long>();
        long avg = 0L;
        for (Long l : differences.values())
            avg += l;
        avg /= differences.size();
        for (Entry<InetSocketAddress, Long> entry : differences.entrySet())
            changeValues.put(entry.getKey(), avg - entry.getValue());
        return changeValues;
    }
}
