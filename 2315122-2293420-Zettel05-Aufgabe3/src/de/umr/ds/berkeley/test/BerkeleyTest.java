package de.umr.ds.berkeley.test;

import org.junit.Test;

import de.umr.ds.berkeley.TimeClient;
import de.umr.ds.berkeley.TimeDaemon;

public class BerkeleyTest {
    
    private static String GROUP_ADDR = "225.225.225.225";

    @Test
    public void testWithoutGUI() {
        new TimeClient("Client 1", 120000, GROUP_ADDR).start();
        new TimeClient("Client 2", -60000, GROUP_ADDR).start();
        new TimeDaemon(GROUP_ADDR).synchronize();
        
        // success if no exception is thrown
    }

}
