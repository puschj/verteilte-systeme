package de.umr.ds.berkeley;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public abstract class TimeDevice {
    
    public static int TIME_PORT = 1055;

    protected InetAddress group;
    
    protected int offset;
    
    protected List<TimeListener> timeListeners;

    public TimeDevice(String groupAddr) {
        try {
            group = InetAddress.getByName(groupAddr);
            offset = 0;
            timeListeners = new LinkedList<TimeListener>();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
    
    public Date getTime() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MILLISECOND, offset);
        return c.getTime();
    }
    
    public Long getTimeInMillis() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MILLISECOND, offset);
        return c.getTimeInMillis();
    }
    
    public void addTimeListener(TimeListener l) {
        timeListeners.add(l);
    }
    
    public void removeTimeListener(TimeListener l) {
        timeListeners.remove(l);
    }
}
