package de.umr.ds.berkeley.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import de.umr.ds.berkeley.TimeClient;
import de.umr.ds.berkeley.TimeDaemon;
import de.umr.ds.berkeley.TimeListener;
import java.awt.Dimension;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

public class DaemonFrame extends JFrame implements TimeListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1955503381658827185L;
    private static String GROUP_ADDR = "225.225.225.225";
    private static int MILLIS_FACTOR = 1000 * 60;
    private JPanel contentPane;
    private JLabel lblCorrection;
    private JTextField txtName;
    private TimeDaemon daemon;
    private JLabel lblTimeValue;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    DaemonFrame frame = new DaemonFrame();
                    frame.initTimeDaemon();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public DaemonFrame() {
        setTitle("Daemon");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 500, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JLabel lblDaemon = new JLabel("DAEMON");
        lblDaemon.setHorizontalAlignment(SwingConstants.CENTER);
        lblDaemon.setFont(new Font("Arial", Font.BOLD, 40));
        contentPane.add(lblDaemon, BorderLayout.NORTH);
        
        lblTimeValue = new JLabel("__.__.____ __:__:__");
        lblTimeValue.setFont(new Font("Consolas", Font.BOLD, 20));
        lblTimeValue.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblTimeValue, BorderLayout.CENTER);
        
        JLabel lblTime = new JLabel("Time:");
        lblTime.setFont(new Font("Arial", Font.BOLD, 20));
        contentPane.add(lblTime, BorderLayout.WEST);
        
        lblCorrection = new JLabel("    ");
        lblCorrection.setOpaque(true);
        lblCorrection.setFont(new Font("Arial", Font.BOLD, 18));
        contentPane.add(lblCorrection, BorderLayout.EAST);
        
        JPanel pnlOptions = new JPanel();
        contentPane.add(pnlOptions, BorderLayout.SOUTH);
        GridBagLayout gbl_pnlOptions = new GridBagLayout();
        gbl_pnlOptions.columnWidths = new int[]{136, 161, 127, 0};
        gbl_pnlOptions.rowHeights = new int[]{0, 31, 33, 0};
        gbl_pnlOptions.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_pnlOptions.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        pnlOptions.setLayout(gbl_pnlOptions);
        
        final JLabel lblName = new JLabel("Name:");
        lblName.setFont(new Font("Arial", Font.PLAIN, 18));
        GridBagConstraints gbc_lblName = new GridBagConstraints();
        gbc_lblName.fill = GridBagConstraints.BOTH;
        gbc_lblName.insets = new Insets(0, 0, 5, 5);
        gbc_lblName.gridx = 0;
        gbc_lblName.gridy = 0;
        pnlOptions.add(lblName, gbc_lblName);
        
        txtName = new JTextField();
        txtName.setHorizontalAlignment(SwingConstants.RIGHT);
        txtName.setFont(new Font("Arial", Font.PLAIN, 18));
        txtName.setMinimumSize(new Dimension(35, 28));
        txtName.setDocument(new LengthRestrictedDocument(10));
        txtName.setToolTipText("insert client name here");
        GridBagConstraints gbc_txtName = new GridBagConstraints();
        gbc_txtName.insets = new Insets(0, 0, 5, 5);
        gbc_txtName.fill = GridBagConstraints.BOTH;
        gbc_txtName.gridx = 1;
        gbc_txtName.gridy = 0;
        pnlOptions.add(txtName, gbc_txtName);
        txtName.setColumns(10);
        
        JLabel lblOffset = new JLabel("Offset (minutes):");
        lblOffset.setFont(new Font("Arial", Font.PLAIN, 18));
        GridBagConstraints gbc_lblOffset = new GridBagConstraints();
        gbc_lblOffset.anchor = GridBagConstraints.WEST;
        gbc_lblOffset.fill = GridBagConstraints.VERTICAL;
        gbc_lblOffset.insets = new Insets(0, 0, 5, 5);
        gbc_lblOffset.gridx = 0;
        gbc_lblOffset.gridy = 1;
        pnlOptions.add(lblOffset, gbc_lblOffset);
        
        final JSpinner sprOffset = new JSpinner();
        sprOffset.setToolTipText("set up the desired time offset here");
        sprOffset.setModel(new SpinnerNumberModel(new Integer(0), null, null, new Integer(1)));
        sprOffset.setFont(new Font("Arial", Font.PLAIN, 18));
        
        JButton btnNewClient = new JButton("New Client");
        btnNewClient.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientFrame cf = new ClientFrame();
                String clientName = txtName.getText();
                TimeClient tc = new TimeClient(clientName, (Integer) sprOffset.getValue() * MILLIS_FACTOR , GROUP_ADDR);
                tc.start();
                cf.setTimeClient(tc);
                cf.setTitle(clientName);
                cf.setVisible(true);
            }
        });
        
        GridBagConstraints gbc_sprOffset = new GridBagConstraints();
        gbc_sprOffset.fill = GridBagConstraints.BOTH;
        gbc_sprOffset.insets = new Insets(0, 0, 5, 5);
        gbc_sprOffset.gridx = 1;
        gbc_sprOffset.gridy = 1;
        pnlOptions.add(sprOffset, gbc_sprOffset);
        btnNewClient.setFont(new Font("Arial", Font.BOLD, 18));
        GridBagConstraints gbc_btnNewClient = new GridBagConstraints();
        gbc_btnNewClient.fill = GridBagConstraints.VERTICAL;
        gbc_btnNewClient.gridheight = 2;
        gbc_btnNewClient.anchor = GridBagConstraints.EAST;
        gbc_btnNewClient.insets = new Insets(0, 0, 5, 0);
        gbc_btnNewClient.gridx = 2;
        gbc_btnNewClient.gridy = 0;
        pnlOptions.add(btnNewClient, gbc_btnNewClient);
        
        JButton btnSync = new JButton("Synchronize");
        btnSync.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                daemon.synchronize();
            }
        });
        btnSync.setFont(new Font("Arial", Font.BOLD, 20));
        GridBagConstraints gbc_btnSync = new GridBagConstraints();
        gbc_btnSync.anchor = GridBagConstraints.NORTH;
        gbc_btnSync.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnSync.gridwidth = 3;
        gbc_btnSync.gridx = 0;
        gbc_btnSync.gridy = 2;
        pnlOptions.add(btnSync, gbc_btnSync);
        setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txtName, sprOffset, btnNewClient, btnSync}));
    }

    @Override
    public void timeUpdated(long correctionValue) {
        boolean negative = correctionValue < 0;
        if (negative) 
            lblCorrection.setForeground(Color.RED);
        else
            lblCorrection.setForeground(new Color(34,139,34));
        lblCorrection.setText((negative ? "" : "+") + correctionValue + "ms");
    }
    
    public void initTimeDaemon() {
        this.daemon = new TimeDaemon(GROUP_ADDR);
        daemon.addTimeListener(this);
        Timer timer = new Timer();
        TimerTask refreshLabel = new TimerTask() {
            private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss:SSS");
            @Override
            public void run() {
                lblTimeValue.setText(format.format(daemon.getTime()));
            }
        };
        timer.scheduleAtFixedRate(refreshLabel, 0, 1);
    }
}
