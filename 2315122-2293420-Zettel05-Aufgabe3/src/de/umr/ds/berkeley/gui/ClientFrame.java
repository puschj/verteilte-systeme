package de.umr.ds.berkeley.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import de.umr.ds.berkeley.TimeClient;
import de.umr.ds.berkeley.TimeListener;

public class ClientFrame extends JFrame implements TimeListener {

    /**
     * 
     */
    private static final long serialVersionUID = -2112672353118812135L;
    private JPanel contentPane;
    private JLabel lblCorrection;
    private JLabel lblTimeValue;
    private TimeClient client;
    private Timer timer = new Timer();
    private JLabel lblName;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientFrame frame = new ClientFrame();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public ClientFrame() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (client != null) {
                    client.stop();
                }
                timer.cancel();
            }
        });
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 175);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        lblName = new JLabel("_");
        lblName.setHorizontalAlignment(SwingConstants.CENTER);
        lblName.setFont(new Font("Arial", Font.BOLD, 40));
        contentPane.add(lblName, BorderLayout.NORTH);
        
        lblTimeValue = new JLabel("__.__.____ __:__:__");
        lblTimeValue.setHorizontalAlignment(SwingConstants.CENTER);
        lblTimeValue.setFont(new Font("Consolas", Font.BOLD, 20));
        contentPane.add(lblTimeValue, BorderLayout.CENTER);
        
        JLabel lblTime = new JLabel("Time:");
        lblTime.setFont(new Font("Arial", Font.BOLD, 20));
        contentPane.add(lblTime, BorderLayout.WEST);
        
        lblCorrection = new JLabel("    ");
        lblCorrection.setOpaque(true);
        lblCorrection.setFont(new Font("Arial", Font.BOLD, 18));
        contentPane.add(lblCorrection, BorderLayout.EAST);
    }

    @Override
    public void timeUpdated(long correctionValue) {
        boolean negative = correctionValue < 0;
        if (negative) 
            lblCorrection.setForeground(Color.RED);
        else
            lblCorrection.setForeground(new Color(34,139,34));
        lblCorrection.setText((negative ? "" : "+") + correctionValue + "ms");
    }
    
    public void setTimeClient(final TimeClient newClient) {
        if (client != null) {
            client.removeTimeListener(this);
            timer.cancel();
        }
        client = newClient;
        client.addTimeListener(this);
        lblName.setText(client.getName());
        TimerTask refreshLabel = new TimerTask() {
            private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss:SSS");
            @Override
            public void run() {
                lblTimeValue.setText(format.format(client.getTime()));
            }
        };
        timer.scheduleAtFixedRate(refreshLabel, 0, 1);
    }
}
