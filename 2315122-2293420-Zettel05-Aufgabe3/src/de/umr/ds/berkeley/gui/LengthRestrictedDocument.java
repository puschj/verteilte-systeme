package de.umr.ds.berkeley.gui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LengthRestrictedDocument extends PlainDocument {

    /**
     * 
     */
    private static final long serialVersionUID = -6873523193405664892L;
    private final int limit;

    public LengthRestrictedDocument(int limit) {
      this.limit = limit;
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a)
        throws BadLocationException {
      if (str == null)
        return;

      if ((getLength() + str.length()) <= limit) {
        super.insertString(offs, str, a);
      }
    }
  }