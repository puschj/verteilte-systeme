package de.umr.ds.berkeley;

public interface TimeListener {
    
    public void timeUpdated(long correctionValue);

}
