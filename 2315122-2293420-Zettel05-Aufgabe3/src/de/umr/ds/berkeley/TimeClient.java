package de.umr.ds.berkeley;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class TimeClient extends TimeDevice {
    
    private String name;
    private boolean stop = false;
    private MulticastSocket mSocket;
    
    public TimeClient(String name, int offset, String groupAddr) {
        super(groupAddr);
            this.name = name;
            this.offset = offset;
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] buf = new byte[1024];
                DatagramPacket rcv = new DatagramPacket(buf, buf.length);
                try {
                    while(!stop) {
                        // receive daemon time
                        mSocket = new MulticastSocket(TIME_PORT);
                        mSocket.joinGroup(group);
                        mSocket.receive(rcv);
                        mSocket.close();
                        long localTime = getTimeInMillis();
                        long daemonTime = Long.parseLong(new String(rcv.getData(), 0, rcv.getLength()));
                        InetAddress deamonAddress = rcv.getAddress();
                        System.out.printf("[%10s] %30s %20d\n", getName(), "Received Deamon time:", daemonTime);
                        System.out.printf("[%10s] %30s %20d\n", getName(), "Local time is:", localTime);
                        
                        // calculate time difference
                        long timeDiff = localTime - daemonTime;
                        
                        // send time difference
                        String diffStr = Long.toString(timeDiff);
                        System.out.printf("[%10s] %30s %20d\n", getName(), "Sending difference to Daemon:", timeDiff);
                        DatagramPacket snd = new DatagramPacket(diffStr.getBytes(), diffStr.length(), deamonAddress, TIME_PORT);
                        DatagramSocket dSocket = new DatagramSocket();
                        dSocket.send(snd);
                        
                        // receive correction value
                        dSocket.receive(rcv);
                        long correctionValue = Long.parseLong(new String(rcv.getData(), 0, rcv.getLength()));
                        System.out.printf("[%10s] %30s %20d\n", getName(), "Received correction value:", correctionValue);
                        dSocket.close();
                        
                        // adjust local time
                        if (correctionValue > Integer.MAX_VALUE || correctionValue < Integer.MIN_VALUE)
                            throw new RuntimeException("correction value overflow");
                        offset += correctionValue;
                        for (TimeListener l : timeListeners)
                            l.timeUpdated(correctionValue);
                        System.out.printf("[%10s] %30s %20d\n", getName(), "Adjusted time is:", getTimeInMillis());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void stop() {
        stop = true;
        mSocket.close();
    }
    
    public String getName() {
        return name;
    }

}
