////////////////////////////////////////////////////
//                    README                      //
//                                                //
//         Verteilte Systeme Zettel 05            //
//                                                //
//                 Joachim Reiß                   //
//                 Jonas Pusch                    //
////////////////////////////////////////////////////

1. Ensure your network setup supports multicasting
2. Execute DaemonFrame.java
3. Add Clients using the GUI
4. Click "Synchronize" to start Berkeley Algorithm
