package de.umr;

import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

public class Main {
    
    public static void main(String[] args) throws Exception {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl("ldap://ldaphost.mathematik.uni-marburg.de");
        contextSource.setUserDn("ou=People,dc=mathematik,dc=uni-marburg,dc=de");
        contextSource.afterPropertiesSet();
        
        LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
        ldapTemplate.authenticate("", "(uid=puschj)", "");
        
//        System.out.println(ldap.search("", "telephonenumber=*", new AttributesMapper() {
//            @Override
//            public Object mapFromAttributes(Attributes arg0) throws NamingException {
//                return arg0.toString();
//            }
//        }));
    }

}
