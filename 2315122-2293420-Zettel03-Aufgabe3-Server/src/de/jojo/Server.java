package de.jojo;

public class Server {

	public final static int ENCRYPT_PORT = 1055;
	public final static int DECRYPT_PORT = 1099;

	public static void main(String args[]) throws Exception {

		new Thread(new CryptService(ENCRYPT_PORT, CryptService.METHOD_ENCRYPT))
				.start();

		new Thread(new CryptService(DECRYPT_PORT, CryptService.METHOD_DECRYPT))
				.start();

		System.out.println("Server started using ports " + ENCRYPT_PORT
				+ " and " + DECRYPT_PORT);

	}
}