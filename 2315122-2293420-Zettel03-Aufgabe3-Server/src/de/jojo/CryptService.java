package de.jojo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import de.umr.encryption.Rot13;

class CryptService implements Runnable {

	public final static int METHOD_ENCRYPT = 0;
	public final static int METHOD_DECRYPT = 1;

	protected int port;
	protected int method;

	private DatagramSocket serverDatagramSocket;
	private byte[] inBuffer;
	private byte[] outBuffer;

	/**
	 * @param port
	 * @param method
	 */
	public CryptService(int port, int method) {
		super();
		this.port = port;
		this.method = method;
	}

	@Override
	public void run() {
		try {
			serverDatagramSocket = new DatagramSocket(port);
			inBuffer = new byte[1024];
			outBuffer = new byte[1024];
			
			while (true) {
				process(method);
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} 
	}

	/**
	 * 
	 * @param cryptMethod
	 *            the method to use for the process. Either
	 *            {@link METHOD_ENCRYPT} or {@link METHOD_DECRYPT}
	 * @return true if successfully processed. False otherwise.
	 */
	public boolean process(int cryptMethod) {

		if (serverDatagramSocket == null || inBuffer == null || outBuffer == null) {
			System.out.println("Service not set up correctly");
			return false;
		}

		try {
			DatagramPacket receivePacket = new DatagramPacket(inBuffer,
					inBuffer.length);

			serverDatagramSocket.receive(receivePacket); // program waits here until
													// receiving

			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();

			String processed = crypt(receivePacket, cryptMethod);

			System.out.println("Request processed to " + processed);

			outBuffer = processed.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(outBuffer,
					outBuffer.length, IPAddress, port);

			serverDatagramSocket.send(sendPacket);
			System.out.println("Response sent");

			return true;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * En- or decrypt the data of the received packet
	 * 
	 * @param receivePacket
	 * @param cryptMethod
	 *            The method to use. 0 for encryption, otherwise decryption
	 * @return a encrypted or decrypted String or "" if no input available
	 */
	private static String crypt(DatagramPacket receivePacket, int cryptMethod) {

		String input = new String(receivePacket.getData());

		System.out.println("input: " + input);

		if (input != null && !input.isEmpty()) {
			if (cryptMethod == 0) {
				return Rot13.encrypt(input);
			}
			return Rot13.decrypt(input);
		}
		return "";
	}

}