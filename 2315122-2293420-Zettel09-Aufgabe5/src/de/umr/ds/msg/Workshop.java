package de.umr.ds.msg;

import java.rmi.Remote;
import java.rmi.RemoteException;

import de.umr.ds.msg.entity.Gift;

public interface Workshop extends Remote {

    public void buildGift(Gift gift) throws RemoteException, InjuryException;
    
}
