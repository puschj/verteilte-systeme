package de.umr.ds.msg;

import java.rmi.Remote;
import java.rmi.RemoteException;

import de.umr.ds.msg.entity.Gift;

public interface PostOffice extends Remote {

    public void packGift(Gift gift) throws RemoteException, InjuryException;
}
