package de.umr.ds.msg;

import java.io.IOException;
import java.util.Random;

import org.codehaus.jackson.map.ObjectMapper;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import de.umr.ds.msg.entity.Gift;

public class WishCenter {

    private static final String TASK_QUEUE_NAME = "task_queue";

    private static final int NUMBER_OF_WISHES = 10;

    private static final String[] GIFTS = { "Rocking-Horse", "Board-Game-Set",
            "LEGO-Construction-Kit", "Huge-Firetruck", "Squirt-Gun",
            "Toy-Railway" };

    public static void main(String[] argv) throws IOException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

        for (int i = 0; i < NUMBER_OF_WISHES; i++) {
            // create random wish
            Random r = new Random();
            Gift gift = new Gift(i, GIFTS[r.nextInt(GIFTS.length)],
                    r.nextInt(4) + 1);
            byte[] message = new ObjectMapper().writeValueAsBytes(gift);
            channel.basicPublish("", TASK_QUEUE_NAME,
                    MessageProperties.TEXT_PLAIN, message);
            System.out.println("[WishCenter] Incoming wish: " + gift);
        }
        channel.close();
        connection.close();
    }
}