package de.umr.ds.msg;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.commons.lang.StringUtils;

public class Main {
    
    private static class Executor implements Runnable {
        private Class<?> clazz;
        private String[] args;
        private Process process;
        
        public Executor(Class<?> clazz, String[] args) {
            this.clazz = clazz;
            this.args = args;
        }
        @Override
        public void run() {
            try {
                String line;
                String javaBin = System.getProperty("java.home")
                        + File.separator + "bin" + File.separator + "java";
                String classPath = System.getProperty("java.class.path");
                String className = clazz.getCanonicalName();
                String argString = StringUtils.join(args, " ");
                ProcessBuilder pb = new ProcessBuilder(javaBin, "-cp",
                        classPath, className, argString);
                process = pb.start();
                BufferedReader input = new BufferedReader(
                        new InputStreamReader(process.getInputStream()));
                while ((line = input.readLine()) != null) {
                    System.out.println(line);
                }
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        public void stop() {
            process.destroy();
        }
    }
    
    private static Executor exec(Class<?> clazz, String ... args) {
        Executor e = new Executor(clazz, args);
        new Thread(e).start();
        return e;
    }

    private static Executor exec(Class<?> clazz) {
       return exec(clazz, "");
    }
    
    public static void main(String[] args) throws InterruptedException {
        // start all relevant processes and store executors in a list
        final List<Executor> executors = new ArrayList<Executor>();
        executors.add(exec(WishCenter.class));
        Thread.sleep(100);
        executors.add(exec(WorkshopImpl.class));
        Thread.sleep(100);
        executors.add(exec(AtelierImpl.class));
        Thread.sleep(100);
        executors.add(exec(PostOfficeImpl.class));
        Thread.sleep(100);
        for (int i = 0; i < 5; i++) {
            executors.add(exec(Elf.class, Integer.toString(i)));
            Thread.sleep(100);
        }
        
        // create simple gui to close all processes at once
        JFrame frame = new JFrame();
        JLabel label = new JLabel("Press to close all processes:");
        JButton close = new JButton("Close");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // kill all started processes
                for (Executor exec : executors) {
                    exec.stop();
                }
                System.exit(0);
            }
        });
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setTitle("DS - Practice 09 / Task 05");
        frame.setLayout(new GridLayout(2,1));
        frame.add(label);
        frame.add(close);
        frame.pack();
        frame.setVisible(true);
    }
}
