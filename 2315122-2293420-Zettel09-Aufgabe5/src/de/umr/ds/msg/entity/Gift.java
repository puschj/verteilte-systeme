package de.umr.ds.msg.entity;

import java.io.Serializable;

public class Gift implements Serializable {
    
    private static final long serialVersionUID = -8183951545119265422L;
    private String name;
    private int complexity;
    private long id;
    
    public Gift() { }
    
    public Gift(long id, String name, int complexity) {
        super();
        this.setId(id);
        this.name = name;
        this.complexity = complexity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getComplexity() {
        return complexity;
    }

    public void setComplexity(int complexity) {
        this.complexity = complexity;
    }

    @Override
    public String toString() {
        return String.format("(%d, %s)", id, name);
    }
}
