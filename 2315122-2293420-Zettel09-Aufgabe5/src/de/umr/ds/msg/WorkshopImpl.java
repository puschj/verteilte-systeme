package de.umr.ds.msg;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.concurrent.Semaphore;

import de.umr.ds.msg.entity.Gift;

public class WorkshopImpl implements Workshop {

    private static final int WORKING_TIME_MULTIPLIER = 1000;

    private static final int NUMBER_OF_WORKBENCHES = 3;

    private Semaphore semaphore = new Semaphore(NUMBER_OF_WORKBENCHES);

    public void buildGift(Gift gift) throws RemoteException, InjuryException {
        try {
            semaphore.acquire();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] building: %s / %d workbenches free\n",
                    "Workshop", gift, available);
            Thread.sleep(gift.getComplexity() * WORKING_TIME_MULTIPLIER);
            if (new Random().nextInt(10) < 2) {
                throw new InjuryException();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] finished: %s / %d workbenches free\n",
                    "Workshop", gift, available);
        }
    }

    public static void main(String[] args) throws RemoteException {
        try {
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        } catch (ExportException e) {
            // do nothing if factory already exists
        }
        Workshop workshop = new WorkshopImpl();
        Workshop stub = (Workshop) UnicastRemoteObject
                .exportObject(workshop, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("workshop", stub);
    }
}
