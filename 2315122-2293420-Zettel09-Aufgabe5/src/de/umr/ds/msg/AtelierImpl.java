package de.umr.ds.msg;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Semaphore;

import de.umr.ds.msg.entity.Gift;

public class AtelierImpl implements Atelier {

    private static final int WORKING_TIME_MULTIPLIER = 500;

    private static final int NUMBER_OF_PAINTING_SETS = 3;

    private Semaphore semaphore = new Semaphore(NUMBER_OF_PAINTING_SETS);

    public void paintGift(Gift gift) throws RemoteException {
        try {
            semaphore.acquire();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] painting: %s / %d painting sets free\n",
                    "Atelier", gift, available);
            Thread.sleep(gift.getComplexity() * WORKING_TIME_MULTIPLIER);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] finished: %s / %d painting sets free\n",
                    "Atelier", gift, available);
        }
    }

    public static void main(String[] args) throws RemoteException {
        try {
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        } catch (ExportException e) {
            // do nothing if factory already exists
        }
        Atelier atelier = new AtelierImpl();
        Atelier stub = (Atelier) UnicastRemoteObject.exportObject(atelier, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("atelier", stub);
    }
}
