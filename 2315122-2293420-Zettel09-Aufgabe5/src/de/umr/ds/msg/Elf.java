package de.umr.ds.msg;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Random;

import org.codehaus.jackson.map.ObjectMapper;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import de.umr.ds.msg.entity.Gift;

public class Elf {

    private static final int MAX_INJURY_TIME = 15000;
    private static final String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] args) throws Exception {
        // get id
        String id = "";
        if (args[0] != null) {
            id = args[0];
        }

        // connect to crafting stations
        Registry registry = LocateRegistry.getRegistry();
        Workshop workshop = (Workshop) registry.lookup("workshop");
        Atelier atelier = (Atelier) registry.lookup("atelier");
        PostOffice postoffice = (PostOffice) registry.lookup("postoffice");

        // set up connection to working queue
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        System.out.printf("[Elf%-7s] Waiting for tasks.\n", id);

        channel.basicQos(1);

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(TASK_QUEUE_NAME, false, consumer);

        while (true) {
            // get task from working queue
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            Gift gift = new ObjectMapper().readValue(delivery.getBody(),
                    Gift.class);
            System.out.printf("[Elf%-7s] Received task: %s.\n", id, gift);
            try {
                // craft and pack gift
                workshop.buildGift(gift);
                atelier.paintGift(gift);
                postoffice.packGift(gift);
            } catch (InjuryException e) {
                // handle possible injury
                channel.basicNack(delivery.getEnvelope().getDeliveryTag(),
                        false, true);
                int injuryTime = new Random().nextInt(MAX_INJURY_TIME);
                System.out.printf("[Elf%-7s] Injured for %dms.\n", id, injuryTime);
                Thread.sleep(injuryTime);
                continue;
            }
            // if not injured, acknowledge gift as done
            System.out.printf("[Elf%-7s] Done: %s\n", id, gift);
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        }
    }
}