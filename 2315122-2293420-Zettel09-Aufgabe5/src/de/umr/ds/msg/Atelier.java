package de.umr.ds.msg;

import java.rmi.Remote;
import java.rmi.RemoteException;

import de.umr.ds.msg.entity.Gift;

public interface Atelier extends Remote {

    public void paintGift(Gift gift) throws RemoteException, InjuryException;
}
