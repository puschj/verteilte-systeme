package de.umr.ds.msg;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.concurrent.Semaphore;

import de.umr.ds.msg.entity.Gift;

public class PostOfficeImpl implements PostOffice {

    private static final int WORKING_TIME_MULTIPLIER = 250;

    private static final int NUMBER_OF_PACK_STATIONS = 3;

    private Semaphore semaphore = new Semaphore(NUMBER_OF_PACK_STATIONS);

    public void packGift(Gift gift) throws RemoteException, InjuryException {
        try {
            semaphore.acquire();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] packing: %s / %d pack stations free\n",
                    "PostOffice", gift, available);
            Thread.sleep(gift.getComplexity() * WORKING_TIME_MULTIPLIER);
            if (new Random().nextInt(10) < 1) {
                throw new InjuryException();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
            int available = semaphore.availablePermits();
            System.out.printf("[%-10s] finished: %s / %d pack stations free\n",
                    "PostOffice", gift, available);
        }
    }

    public static void main(String[] args) throws RemoteException {
        try {
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        } catch (ExportException e) {
            // do nothing if factory already exists
        }
        PostOffice postoffice = new PostOfficeImpl();
        PostOffice stub = (PostOffice) UnicastRemoteObject.exportObject(
                postoffice, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("postoffice", stub);
    }
}
