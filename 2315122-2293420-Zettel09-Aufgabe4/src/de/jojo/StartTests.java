package de.jojo;

/**
 * 
 */

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author joachim
 * 
 */
public class StartTests {

	public final static int MIN_WORKTIME_SECONDS = 1;
	public final static int MAX_WORKTIME_SECONDS = 3;

	public final static int NO_ALL_WORKERS = 10;
	public final static int NO_ACTIVE_WORKERS = 3;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Aufgabe a
		// Implementation of MyFixedThreadPool

		// Aufgabe b
		doTest(new MyFixedThreadPool(NO_ACTIVE_WORKERS));

		// Aufgabe c
		doTest(Executors.newFixedThreadPool(NO_ACTIVE_WORKERS));

	}

	public static void doTest(Executor executor) {

		ArrayList<Worker> workers = new ArrayList<Worker>();

		for (int i = 0; i < NO_ALL_WORKERS; i++) {

			workers.add(new Worker(MIN_WORKTIME_SECONDS, MAX_WORKTIME_SECONDS));

		}

		System.out
				.println("Starting executing threads with MyFixedThreadPool...");

		for (Worker w : workers) {
			executor.execute(w);
		}
	}

}
