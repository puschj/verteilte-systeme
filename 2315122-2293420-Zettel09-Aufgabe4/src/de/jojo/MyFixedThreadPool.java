package de.jojo;

import java.util.concurrent.Executor;

/**
 * Aufgabe a
 * 
 * @author joachim
 * 
 */
public class MyFixedThreadPool implements Executor {

	private static int currentlyActiveRunnables = 0;

	public int maxThreads;

	public MyFixedThreadPool(int maxThreads) {
		this.maxThreads = maxThreads;
	}

	@Override
	public void execute(Runnable command) {
		new MyThread(command).start();
	}

	class MyThread extends Thread {

		private Runnable runnable;

		public MyThread(Runnable runnable) {
			this.runnable = runnable;
		}

		@Override
		public void run() {
			do {
				try {
					if (currentlyActiveRunnables < maxThreads) {

						currentlyActiveRunnables++;

						runnable.run();

						currentlyActiveRunnables--;

						break;
					} else {
						final long threadId = Thread.currentThread().getId();
						
						System.out.println("Could not start work for thread '" + threadId
								+ "' yet. Pool was too busy.. Waiting..");
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} while (true);
		}
	}
}
