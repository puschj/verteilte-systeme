package de.jojo;

import java.util.Random;

public class Worker implements Runnable {

	private int minSeconds = 3;

	private int maxSeconds = 10;

	public Worker(int minSeconds, int maxSeconds) {
		super();
		this.minSeconds = minSeconds;
		this.maxSeconds = maxSeconds;
	}

	@Override
	public void run() {
		final long workTime = getSomeWorkTime(minSeconds, maxSeconds);
		final long threadId = Thread.currentThread().getId();
		try {
			System.out.println("The thread '" + threadId
					+ "' is now working for " + workTime / 1000 + " seconds");

			Thread.sleep(workTime);

			System.out.println("work done in Thread '" + threadId + "'");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get some worktime within the range of minSeconds and maxSeconds
	 * 
	 * @param minSeconds
	 * @param maxSeconds
	 * @return a time in ms
	 */
	private static long getSomeWorkTime(int minSeconds, int maxSeconds) {
		Random r = new Random();
		return (r.nextInt(maxSeconds - minSeconds + 1) + minSeconds) * 1000;
	}
}
