package de.umr.ds.rpc;

public class Constants {

    public static final int METHOD_ADD = 0;
    public static final int METHOD_SUB = 1;
    public static final int METHOD_MUL = 2;
    public static final int METHOD_DIV = 3;
    public static final int METHOD_MOD = 4;
    
    public static final int METHOD_STRINGCOMPARE = 10;
    
    public static final int MSG_OK = 200;
    public static final int MSG_ERR = 400;
    
}