package de.umr.ds.rpc.test;

import static org.junit.Assert.*;

import org.junit.Test;

import de.umr.ds.rpc.StringComparator;
import de.umr.ds.rpc.impl.StringComparatorImpl;

public class StringComparatorImplTest {

    @Test
    public void testStringsEqual() {
        StringComparator sc = new StringComparatorImpl();
        assertTrue(sc.stringsEqual("", ""));
        assertTrue(sc.stringsEqual("Hallo", "Hallo"));
        assertFalse(sc.stringsEqual("Hallo", "Welt"));
        assertFalse(sc.stringsEqual("Test", "TEst"));
    }

}
