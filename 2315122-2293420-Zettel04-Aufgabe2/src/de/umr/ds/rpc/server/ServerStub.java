package de.umr.ds.rpc.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;

public interface ServerStub {
    
    public void processMethodCall(DataInputStream is, DataOutputStream os);
    
    public String getSupportedClassName();

}
