package de.umr.ds.rpc.server;

import static de.umr.ds.rpc.Constants.METHOD_STRINGCOMPARE;
import static de.umr.ds.rpc.Constants.MSG_ERR;
import static de.umr.ds.rpc.Constants.MSG_OK;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.umr.ds.rpc.StringComparator;

public class StringComparatorServerStub implements ServerStub {
    
    private StringComparator strcomp;
    
    public StringComparatorServerStub(StringComparator strcomp) {
        this.strcomp = strcomp;
    }
    
    @Override
    public void processMethodCall(DataInputStream is, DataOutputStream os) {
        try {
            int method;
            String s1, s2;
            boolean res;
            try {
                method = is.readInt();
                s1 = is.readUTF();
                s2 = is.readUTF();     
                
                switch (method) {
                    case METHOD_STRINGCOMPARE: res = strcomp.stringsEqual(s1, s2); break;
                    default: throw new IllegalArgumentException("invalid method type: "+method);
                };
                
            } catch (Exception e) {
                os.writeInt(MSG_ERR);
                os.writeUTF(e.getMessage());
                os.flush();
                return;
            }
            os.writeInt(MSG_OK);
            os.writeBoolean(res);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getSupportedClassName() {
        return StringComparator.class.getSimpleName();
    }
}


