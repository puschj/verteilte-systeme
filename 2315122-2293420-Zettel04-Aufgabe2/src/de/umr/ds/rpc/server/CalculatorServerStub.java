package de.umr.ds.rpc.server;

import static de.umr.ds.rpc.Constants.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.umr.ds.rpc.Calculator;

public class CalculatorServerStub implements ServerStub {
    
    private Calculator calc;
    
    public CalculatorServerStub(Calculator calc) {
        this.calc = calc;
    }
    
    @Override
    public void processMethodCall(DataInputStream is, DataOutputStream os) {
        try {
            int method, x, y, res = 0;
            try {
                method = is.readInt();
                x = is.readInt();
                y = is.readInt();
                
                switch (method) {
                    case METHOD_ADD: res = calc.add(x, y); break;
                    case METHOD_SUB: res = calc.sub(x, y); break;
                    case METHOD_MUL: res = calc.mul(x, y); break;
                    case METHOD_DIV: res = calc.div(x, y); break;
                    case METHOD_MOD: res = calc.mod(x, y); break;
                    default: throw new IllegalArgumentException("invalid method type: "+method);
                };
                
            } catch (Exception e) {
                os.writeInt(MSG_ERR);
                os.writeUTF(e.getMessage());
                os.flush();
                return;
            }
            os.writeInt(MSG_OK);
            os.writeInt(res);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getSupportedClassName() {
        return Calculator.class.getSimpleName();
    }
}


