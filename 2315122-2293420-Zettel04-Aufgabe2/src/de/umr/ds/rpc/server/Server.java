package de.umr.ds.rpc.server;

import static de.umr.ds.rpc.Constants.MSG_ERR;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    
    private static final int PORT = 1055;
    
    private List<ServerStub> serverStubs = new ArrayList<ServerStub>();
    
    ServerSocket serverSocket;
    
    public void addServerStub(ServerStub serverStub) {
        serverStubs.add(serverStub);
    }
    
    public void removeServerStub(ServerStub serverStub) {
        serverStubs.remove(serverStub);
    }
    
    private class CallHandler implements Runnable {
        
        private DataInputStream is;
        private DataOutputStream os;
        
        public CallHandler(Socket socket) throws IOException {
            os = new DataOutputStream(socket.getOutputStream());
            is = new DataInputStream(socket.getInputStream());
        }

        @Override
        public void run() {
            String className;
            try {
                className = is.readUTF();
                for (ServerStub serverStub: serverStubs) {
                    if (serverStub.getSupportedClassName().equals(className)) {
                        serverStub.processMethodCall(is, os);
                        return;
                    }
                    os.writeInt(MSG_ERR);
                    os.writeUTF("no server stub registered for '"+className+"'");
                    os.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(PORT);
                    while(true) {
                        Socket socket = serverSocket.accept();
                        System.out.println("connection engaged: " + socket);
                        new Thread(new CallHandler(socket)).start();
                    }
                } catch (IOException e) {
                    System.out.println("server closed (reason: \""+e.getMessage()+"\")");
                }
            }
        }).start();
    }
    
    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
