package de.umr.ds.rpc.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import static de.umr.ds.rpc.Constants.*;

import de.umr.ds.rpc.Calculator;

public class CalculatorClientStub implements Calculator {

    private static final int PORT = 1055;
    
    private static final String ADDRESS = "localhost";
    
    public CalculatorClientStub() {
        
    }
    
    @Override
    public int add(int x, int y) {
        return sendAndReceiveProcedureCall(METHOD_ADD, x, y);
    }

    @Override
    public int sub(int x, int y) {
        return sendAndReceiveProcedureCall(METHOD_SUB, x, y);
    }

    @Override
    public int mul(int x, int y) {
        return sendAndReceiveProcedureCall(METHOD_MUL, x, y);
    }

    @Override
    public int div(int x, int y) {
        return sendAndReceiveProcedureCall(METHOD_DIV, x, y);
    }

    @Override
    public int mod(int x, int y) {
        return sendAndReceiveProcedureCall(METHOD_MOD, x, y);
    }
    
    private int sendAndReceiveProcedureCall(int method, int x, int y) {
        Socket socket = null;
        DataOutputStream os = null;
        DataInputStream is = null;
        int res = 0;
        try {
            // setup connection
            socket = new Socket(ADDRESS, PORT);
            os = new DataOutputStream(socket.getOutputStream());
            is = new DataInputStream(socket.getInputStream());
            
            // send
            os.writeUTF(Calculator.class.getSimpleName());
            os.writeInt(method);
            os.writeInt(x);
            os.writeInt(y);
            os.flush();
            
            // receive
            int check = is.readInt();
            if (check == MSG_OK)
                res = is.readInt();
            else
                throw new RuntimeException(is.readUTF());
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // close connection
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

}
