package de.umr.ds.rpc.client;

import de.umr.ds.rpc.Calculator;
import de.umr.ds.rpc.StringComparator;

public class Client {
    
    public static void main(String[] args) {
        Calculator c = new CalculatorClientStub();
        System.out.println(c.add(5, 6));
        System.out.println(c.div(10, 7));
        
        StringComparator s = new StringComparatorClientStub();
        System.out.println(s.stringsEqual("Test", "TEst"));
        System.out.println(s.stringsEqual("Verteilte_Systeme", "Verteilte_Systeme"));
    }
}
