package de.umr.ds.rpc.client;

import static de.umr.ds.rpc.Constants.METHOD_STRINGCOMPARE;
import static de.umr.ds.rpc.Constants.MSG_OK;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import de.umr.ds.rpc.StringComparator;

public class StringComparatorClientStub implements StringComparator {

    private static final int PORT = 1055;
    
    private static final String ADDRESS = "localhost";
    
    public StringComparatorClientStub() {
        
    }

    @Override
    public boolean stringsEqual(String s1, String s2) {
        return sendAndReceiveProcedureCall(METHOD_STRINGCOMPARE, s1, s2);
    }
    
    private boolean sendAndReceiveProcedureCall(int method, String s1, String s2) {
        Socket socket = null;
        DataOutputStream os = null;
        DataInputStream is = null;
        boolean res = false;
        try {
            // setup connection
            socket = new Socket(ADDRESS, PORT);
            os = new DataOutputStream(socket.getOutputStream());
            is = new DataInputStream(socket.getInputStream());
            
            // send
            os.writeUTF(StringComparator.class.getSimpleName());
            os.writeInt(method);
            os.writeUTF(s1);
            os.writeUTF(s2);
            os.flush();
            
            // receive
            int check = is.readInt();
            if (check == MSG_OK)
                res = is.readBoolean();
            else
                throw new RuntimeException(is.readUTF());
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // close connection
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

}
