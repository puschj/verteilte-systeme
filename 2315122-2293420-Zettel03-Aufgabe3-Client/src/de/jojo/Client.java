package de.jojo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

class Client {

	public final static int ENCRYPT_PORT = 1055;
	public final static int DECRYPT_PORT = 1099;

	public final static String HOSTNAME = "localhost";

	private static InetAddress ipAdr;
	private static byte[] sendData;
	private static byte[] receiveData;
	private static DatagramSocket clientSocket;

	public static void main(String args[]) throws Exception {

		ipAdr = InetAddress.getByName(HOSTNAME);
		sendData = new byte[1024];
		receiveData = new byte[1024];

		print("Please enter text to be encrypted");
		String input = new BufferedReader(new InputStreamReader(System.in))
				.readLine();

		if (input == null || input.isEmpty()) {
			print("No input was given.");
			return;
		}

		// encryption
		String encryptedInput = sendAndReceive(input, ENCRYPT_PORT);
		if (encryptedInput.isEmpty()) {
			print("Received empty response..");
			return;
		}
		print("Using encryption service of the server resulted in: "
				+ encryptedInput);

		// decryption
		String decryptedInput = sendAndReceive(encryptedInput, DECRYPT_PORT);
		if (decryptedInput.isEmpty()) {
			print("Received empty response..");
			return;
		}
		print("Using decryption service of the server results in: "
				+ decryptedInput);

		print("Good bye.");
	}

	private static String sendAndReceive(String text, int port) {

		sendData = text.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData,
				sendData.length, ipAdr, port);

		try {
			clientSocket = new DatagramSocket();

			clientSocket.send(sendPacket);

			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);

			clientSocket.receive(receivePacket);

			String received = new String(receivePacket.getData());

			return received;

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			clientSocket.close();
		}

		return "";
	}

	private static void print(String text) {
		System.out.println(text + "\n");
	}
}